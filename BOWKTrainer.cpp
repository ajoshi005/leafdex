//BOWKmeans training- Creates BOW Descriptors


#include <highgui.h>
#include "opencv2/opencv.hpp"
#include <string>
//#include "trainer.hpp"
#include <iostream>
#include "HarrisDetector.cpp"

using namespace cv;
using namespace std;

int main(int argc, char** argv)
{
	
	map<string,Mat> class_data;
	
	string fname;
	Mat trainDesc;
	
	char opt = 'y';
	while(opt!='n')
	{
		cout<<"\n Enter filename";
		cin>>fname;
	
		FileStorage fs2(fname,FileStorage::READ);
		Mat add1;
		fs2["desc_all"] >> add1;
		cout<<"\n"<<add1.rows;
		trainDesc.push_back(add1);
	
		cout<<"\n Add another file?(y/n)";
		cin>>opt;
	}
	cout<<"\n Train desc"<<trainDesc.rows;
	BOWKMeansTrainer bowtrainer(500);
	bowtrainer.add(trainDesc);
	cout<<"\n clustering Bow features"<<endl;
	
	Mat vocabulary = bowtrainer.cluster();
	
	FileStorage fs1("vocabulary.xml",FileStorage::WRITE);
	
	fs1<<"vocabulary"<<vocabulary;
	
	fs1.release();
	
	
}
