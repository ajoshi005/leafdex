LEAFDEX:
========
 
This document describes the process pipeline for recognition of leaves. 
The current prototype uses the following pipeline:

Training
--------

We are currently using the [Flavia dataset](http://flavia.sourceforge.net/) for training the svm model. The crux of the recognition system is the Bag-Of-Words Model with SIFT features. 

The steps for training the SVM classifier is as follows:

* Create separate folders of each species/subspecies with a textfile of a list of the images(Preferably .jpg).Each image path         should be on a separate line. This is easily achieved with the 'ls >> filename' command on linux or cygwin on windows.
* Run the sift.cpp code to get an .xml file with the SIFT features.
* Copy the parent directory.
* Run BOWk program and type in the filenames when prompted.
* run BOWd to get the descriptors
* Run svmTrain to train the classifier

This classifier can then be used for testing

Currently, the classifier has been trained for 3 species which it is easily able to classify. Further work will include simplifying the training process and adding more species.
