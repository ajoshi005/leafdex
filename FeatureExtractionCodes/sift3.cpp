//
#include <cv.h>
#include <highgui.h>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <string>
#include <fstream>
#include "HarrisDetector.cpp"

using namespace std;
using namespace cv;

int main()
{
	Mat img;
	Mat threshImg;
    Mat desc_all;
	//adaptiveThreshold(img, threshImg);
	
	ifstream f1;
	f1.open("Leaf3");
	if(f1.is_open()==0)
	{
		cout<<"\n Input File does not exist!";
		exit(0);
	}
	string imgName;
    while(!f1.eof())
    {
			vector<KeyPoint> keypoints;
		    Mat desc;
            f1>>imgName;
			cout<<"\n"<<imgName;
			img=imread(imgName);
			if(!img.data)
			{	
				cout<<"\n File not found!";		
				continue;
			}
			cvtColor(img,img,CV_BGR2GRAY);
			SIFT sift;
			
			vector<Point2f> points;
			//harrisFeatures(img, points);
			cout<<"\n POints size:"<<points.size();
			int i;
			/*
			for(i=0;i<points.size();i++)
			{
				KeyPoint temp(points[i],10,-1,0,0,-1);
				keypoints.push_back(temp);
				cout<<"\n Point "<<i<<" "<< keypoints[i].pt.x <<" "<<keypoints[i].pt.y; 
			}
			*/	
			sift(img,img,keypoints,desc,false);
			desc_all.push_back(desc);

          
	
	}
	
    FileStorage fs("leaf3.xml",FileStorage::WRITE);

    fs<<"desc_all"<<desc_all;
    	
    fs.release();	
	return 1;
}
