#include <iostream>
#include <cv.h>
#include <highgui.h>
#include "opencv2/ml/ml.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/legacy/legacy.hpp"
#include <string>
#include <fstream>
#define LABELS 2
using namespace cv;
using namespace cvflann;
using namespace std;

int main()
{
	
	/* code for reading file with SIFT features*/

	Mat trainData,labels;
	
	int count = 0;
	chdir("Leaf1");
	FileStorage fs1("Leaf1Response.xml",FileStorage::READ);
	Mat resResponse;
	fs1["responseHist"]>>resResponse;
	count=resResponse.rows;
	
	trainData.push_back(resResponse);
	Mat reslabels=Mat::zeros(count,1,CV_32SC1);
	
	labels.push_back(reslabels);
	
	cout<<"trainData:"<<trainData.rows<<","<<trainData.cols<<"\n Label:"<<labels.rows;
	fs1.release();
	
	chdir("../Leaf2");
	FileStorage fs2("Leaf2Response.xml",FileStorage::READ);
	Mat capResponse;
	fs2["responseHist"]>>capResponse;
	Mat caplabels=Mat::ones(capResponse.rows, 1, CV_32SC1);
	trainData.push_back(capResponse);
	labels.push_back(caplabels);
	
	
	chdir("../Leaf3");
	FileStorage fs3("Leaf3Response.xml",FileStorage::READ);
	Mat indResponse;
	fs3["responseHist"]>>indResponse;
	Mat indlabels=Mat::ones(indResponse.rows, 1, CV_32SC1);
	indlabels.setTo((float)2);
	trainData.push_back(indResponse);
	labels.push_back(indlabels);
	chdir("..");

	cout<<"\ntrain:\n"<<trainData;
	
	cout<<"\n LAbels:\n"<<labels;
	

	
	
	
	
	CvSVMParams params;
	params.svm_type = CvSVM::C_SVC;
	params.kernel_type = LINEAR;
	params.term_crit =  cvTermCriteria(CV_TERMCRIT_ITER, 100, 1e-6);
	
	//Train array:DD array with responsehistograms values
	
	Mat svmData;
	trainData.convertTo(svmData,CV_32F);
	Mat labelData;
	labelData=labels.t(); //labels: Array of labels
	
	

	//SVM training:
	
	CvSVM svm;
	params=svm.get_params();
	
	svm.train_auto(svmData, labelData, Mat(), Mat(), params, 10);
	svm.save("classifier.xml","circuit");
	
	//End of training

	
	return 0;
	
}
	
	
