//SVM test

#include <iostream>
#include <cv.h>
#include <highgui.h>
#include "opencv2/ml/ml.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/legacy/legacy.hpp"
#include <string>
#include <fstream>
//#include "HarrisDetector.cpp"
//#include "highDensity.cpp"

using namespace cv;
using namespace std;

int main()
{

//Test
	CvSVM svm;
	svm.load("classifier.xml","circuit");
	//Mat testImg = imread("Test");
	
	Mat vocabulary;
	
	FileStorage fs3("vocabulary.xml", FileStorage::READ);
	cout<<"Reading Vocabulary from file";
	
	fs3["vocabulary"]>>vocabulary;
	fs3.release();
	chdir("Test");
	ifstream f1;
	f1.open("test");
	
	while(!f1.eof())
	{
		char imgName[20];
		
		f1>>imgName;
		cout<<"\n"<<imgName;
		int i=0;
		
	
		Mat img=imread(imgName);
		Mat imgCopy(img);
		if(!img.data)
			continue;
		Ptr<FeatureDetector> detector(new SiftFeatureDetector());
		Ptr<DescriptorExtractor> extractor(new SiftDescriptorExtractor());
		Ptr<DescriptorMatcher> matcher(new BruteForceMatcher<L2 <float> >() );
		BOWImgDescriptorExtractor bowide(extractor,matcher);
		bowide.setVocabulary(vocabulary);
		vector<KeyPoint> keypoints;
		Mat desc(0,30,CV_64FC1);
		Mat responseHist;
		cvtColor(img,img,CV_BGR2GRAY);
		SIFT sift;
		vector<Point2f> points;
		//harrisFeatures(img, points);
		//cout<<"\n POints size:"<<points.size();
		
		/*
		for(i=0;i<points.size();i++)
		{
				KeyPoint temp(points[i],10,-1,0,0,-1);
				keypoints.push_back(temp);
				//cout<<"\n Point "<<i<<" "<< keypoints[i].pt.x <<" "<<keypoints[i].pt.y; 
		}
        */
		sift(img,img,keypoints,desc,false);
		bowide.compute(img,keypoints,responseHist);
		float predLabel=5;
		if(responseHist.rows!=0)
		{
			predLabel = svm.predict(responseHist, false);
			if(predLabel == 0)
			{
				putText(imgCopy,"Leaf1",Point(img.cols/2-10,img.rows-150),FONT_HERSHEY_PLAIN, 8,Scalar(0,0,255));
			}
			else if(predLabel == 1)
			{
				putText(imgCopy,"Leaf2",Point(img.cols/2-10,img.rows-150),FONT_HERSHEY_PLAIN, 8,Scalar(0,0,255));
			}
			else if(predLabel == 2)
			{
				putText(imgCopy,"Leaf3",Point(img.cols/2-10,img.rows-150),FONT_HERSHEY_PLAIN, 8,Scalar(0,0,255));
			}
		}	
		cout<<"\n value:"<<predLabel;
        int imgNo;
        char check[20];
        strncpy(check,imgName,4);
        imgNo=atoi(check);
        if(imgNo >= 1195 && imgNo <= 1267)
        {
            if(predLabel == 0)
            {
                putText(imgCopy,"Correct",Point(100,120),FONT_HERSHEY_PLAIN, 8,Scalar(0,255,0));
            }
            else
            {
                putText(imgCopy,"Wrong",Point(100,120),FONT_HERSHEY_PLAIN, 8,Scalar(0,0,255));
            }
        }
        if(imgNo >= 3111 && imgNo <= 3175)
        {
            if(predLabel == 1)
            {
                putText(imgCopy,"Correct",Point(100,120),FONT_HERSHEY_PLAIN, 8,Scalar(0,255,0));
            }
            else
            {
                putText(imgCopy,"Wrong",Point(100,120),FONT_HERSHEY_PLAIN, 8,Scalar(0,0,255));
            }
        }    
		if(imgNo >= 1324 && imgNo <= 1385)
        {
            if(predLabel == 2)
            {
                putText(imgCopy,"Correct",Point(100,120),FONT_HERSHEY_PLAIN, 8,Scalar(0,255,0));
            }
            else
            {
                putText(imgCopy,"Wrong",Point(100,120),FONT_HERSHEY_PLAIN, 8,Scalar(0,0,255));
            }
        } 
		chdir("../Output");
		imwrite(imgName,imgCopy);
		chdir("../Test");
	}	
	return 0;
}	
