//BOWKMeans Recogniser

#include <iostream>
#include <cv.h>
#include <highgui.h>
#include "opencv2/ml/ml.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/legacy/legacy.hpp"
#include <string>
#include <fstream>
#include <vector>
#include "HarrisDetector.cpp"

using namespace cv;
using namespace std;


int main()
{
	Mat vocabulary;
	
	FileStorage fs1("vocabulary.xml", FileStorage::READ);
	cout<<"Reading Vocabulary from file";
	
	fs1["vocabulary"]>>vocabulary;
	fs1.release();
	Mat img;
	Ptr<FeatureDetector> detector(new SiftFeatureDetector);
	
	Ptr<DescriptorExtractor> extractor(new SiftDescriptorExtractor);
	Ptr<DescriptorMatcher> matcher(new BruteForceMatcher<L2 <float> > );
	BOWImgDescriptorExtractor bowide(extractor,matcher);
	bowide.setVocabulary(vocabulary);
	vector<KeyPoint> keypoints;
	Mat desc,desc1;
	//vector<Mat> desc;
	//Setting up training data
	char opt='y';
	int i=0;
	char compName[50],response[50];
	while(opt!='n')
	{
		cout<<"\n Enter Component for training";
		cin>>compName;
	
		strcpy(response,compName);
		
		strcat(response,"Response.xml");
		cout<<"\n Adding to "<<response;
		chdir(compName);
		
		string imgName;
		ifstream f1;
		f1.open(compName);
		
		FileStorage fs2(response, FileStorage::WRITE);
		//int flag =0;
		Mat desc;
		//Mat responseCopy;
		while(!f1.eof())
		{
			Mat responseHist(1,30,CV_32FC1);
			f1>>imgName;
			cout<<"\n"<<imgName;
			img=imread(imgName);
			if(!img.data)
				continue;
			cvtColor(img,img,CV_BGR2GRAY);
			SIFT sift;
			vector<Point2f> points;
			//harrisFeatures(img, points);
			//cout<<"\n POints size:"<<points.size();
			/*
		
			for(i=0;i<points.size();i++)
			{
				KeyPoint temp(points[i],10,-1,0,0,-1);
				keypoints.push_back(temp);
				//cout<<"\n Point "<<i<<" "<< keypoints[i].pt.x <<" "<<keypoints[i].pt.y; 
			}
			*/	
			sift(img,img,keypoints,desc1,false);
			
			bowide.compute(img,keypoints,responseHist);
			
			int i;
			cout<<"\n ResponseHist:"<<responseHist.rows<<","<<responseHist.cols<<","<<responseHist.type()<<","<<responseHist.channels();
			//cout<<"\n"<<desc.cols;
			
			if(responseHist.rows!=0)
			{ 
				
				desc.push_back(responseHist);
				
			}
			//cout<<"\n "<<desc.rows<<" "<<desc.cols;
			
		}
		
		fs2<<"responseHist"<<desc;
		
		fs2.release();
		
		f1.close();
		//f2.close();
		
		cout<<"\nAnother comp?(y/n)";
		cin>>opt;
		chdir("..");
		
	}
    return 0;
}
		
		
	
	
	

