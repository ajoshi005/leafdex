//SVM test

#include <iostream>
#include <cv.h>
#include <highgui.h>
#include "opencv2/ml/ml.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/legacy/legacy.hpp"
#include <string>
#include <fstream>
//#include "HarrisDetector.cpp"
//#include "highDensity.cpp"

using namespace cv;
using namespace std;

int main()
{

//Test
	CvSVM svm;
	svm.load("classifier.xml","circuit");
	//Mat testImg = imread("Test");
	
	Mat vocabulary;
	
	FileStorage fs3("vocabulary.xml", FileStorage::READ);
	cout<<"Reading Vocabulary from file";
	
	fs3["vocabulary"]>>vocabulary;
	fs3.release();
	chdir("Test");
	ifstream f1;
	f1.open("test");
	
	while(!f1.eof())
	{
		string imgName;
		f1>>imgName;
		cout<<"\n"<<imgName;
		Mat img=imread(imgName);
		if(!img.data)
			continue;
		Ptr<FeatureDetector> detector(new SiftFeatureDetector());
		Ptr<DescriptorExtractor> extractor(new SiftDescriptorExtractor());
		Ptr<DescriptorMatcher> matcher(new BruteForceMatcher<L2 <float> >() );
		BOWImgDescriptorExtractor bowide(extractor,matcher);
		bowide.setVocabulary(vocabulary);
		vector<KeyPoint> keypoints;
		Mat desc(0,30,CV_64FC1);
		Mat responseHist;
		cvtColor(img,img,CV_BGR2GRAY);
		SIFT sift;
		vector<Point2f> points;
		//harrisFeatures(img, points);
		//cout<<"\n POints size:"<<points.size();
		int i;
		/*
		for(i=0;i<points.size();i++)
		{
				KeyPoint temp(points[i],10,-1,0,0,-1);
				keypoints.push_back(temp);
				//cout<<"\n Point "<<i<<" "<< keypoints[i].pt.x <<" "<<keypoints[i].pt.y; 
		}
        */
		sift(img,img,keypoints,desc,false);
		bowide.compute(img,keypoints,responseHist);
		
		float predLabel=5;
		if(responseHist.rows!=0)
		{
			predLabel = svm.predict(responseHist, false);
		}
		cout<<"\n value:"<<predLabel;
	}	
	return 0;
}	
